## Kalkulačka
Vaším prvním úkolem bude vytvoření plnohodnotné kalkulačky.

### Zadání

Vytvořte konzolovou aplikaci, která bude umět následující funkce.

- Sčítání (addition 2)
- Odčítání (subtraction 2)
- Násobení (multiplication 2)
- Dělení (division 2)
- Výpočet modula (modulo 2, v našem případě mohou být použita pouze varianty při kterých platí x >= y a y > 0)
- Výpočet druhé mocniny čísly (secondPower 1)
- Výpočet n-té mocniny čísla (power 2, pořadí: mocněnec, mocnitel, v našem případě mohou být použiti pouze kladní mocnitelé (nula je kladná)) - vracejte typ float.
- Výpočet druhé odmocniny z čísla (secondRadix 1, odmocninu lze provést pouze z kladného celého čísla splňující: x > 0)
- Magický výpočet (magic 4) - operace bude vysvětlena

Každá z výše uvedených funkcí vrací na výstupu výpočet. V případě, že výpočet nelze provést, tak funkce informuje uživatele hláškou.

#### Magický výpočet
Na vstupu přijímá 4 parametry x, y, z, k. Funkce nejprve spočítá x + k = l, y + z = m a pote vypočte ((l / m) + 1) = n. Hodnotu v proměnné n vrátí na výstupu.

### Testování
Po odladění pusťte aplikaci přes cmd, kde vstupní hodnoty vezmete ze souboru input.txt a výstupní hodnoty zapíšete do ouput.txt. **Neznamená to, že použijete StreamReader nebo StreamWriter!**



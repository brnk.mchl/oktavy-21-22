# Oktavy 21-22

GYMVOD programovací seminář Oktavy 2021/2022

## Učitelé

**David Hájek**

- hajekdd@gmail.com
- FEL
- Java, php , python, javascript


### TEST

### Soutěž Kasiopea
- https://kasiopea.matfyz.cz/


### Pravidla

- Všichni se naučíme pracovat s GITem. Materiály budou na GitLabu.
- Discord
- Vždycky si to z něho na začátku hodiny stáhneme.
- Úkoly budou každý týden nebo ob týden. Na vypracování máte týden,
s dobrou výmluvou máte dva týdny. Jinak dostanete úkol navíc. Za
úkoly se budou dostávat jedničky. Neopisovat, poznám to. Odevzdávat
na svůj repositář.
- Budete mít projekt na semestr ve dvojicích. Témata si představíme
příští hodinu.
- Písemky budou asi dvě - část teoretická a část praktická.
- Při hodině budete spolupracovat.

## Maturita
- [ ] Základy algoritmizace a programování
- [ ] Datové typy a struktury programovacího jazyka
- [ ] Cyklus a rekurze
- [ ] Seznamy
- [ ] Práce s textem a soubory
- [ ] Algoritmy třídění a řazení
- [ ] Numerické algoritmy – aritmetika čísel
- [ ] Uživatelské rozhraní programu
- [ ] Objektově orientované programování
- [ ] Grafika v jazyce C#


### Harmonogram

| Datum | Učitel | Téma | Přednášky | Pokročilé | Úkol |
| --- | --- | --- | --- | --- | --- |
| 8.9.2020 | David | Úvod; seznámení; stránka; úkoly; semestrálka; postup při řešení problému; vlastnosti algoritmu, základní struktury; basic úloha | [lec01](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2021.09.15/uvodni-prezentace.pdf) | | |
| 15.9.2020 | David | GIT, založit repo, nasdílet, ssh klíč; pull, add ., commit, push, rebase, merge | [lec02](https://gitlab.com/gymvod-group/oktavy-21-22/-/blob/main/lectures/2021.09.15/prezentace02.pdf) | [adv01](https://gitlab.com/gymvod-group/oktavy-21-22/-/tree/main/advanced/2021.09.15) | |

